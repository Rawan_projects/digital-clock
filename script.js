const hour =document.getElementById("hours");
const min =document.getElementById("minutes");
const sec =document.getElementById("seconds");
const ampm =document.getElementById("ampm");
updateClock();

function updateClock(){
let h= new Date().getHours()
let m= new Date().getMinutes()
let s= new Date().getSeconds()
let AMPM = "AM";

// TO MAKE THE NUMBER FROM 1 TO 11 RATHER THAN 13,14...
if(h>12){
    h=h-12;
    AMPM="PM";
}

if(h<10){
    h= "0" + h;
}
if(m<10){
    m= "0" + m;
}
if(s<10){
    s= "0" + s;
}

hour.innerText=h;
min.innerText=m;
sec.innerText=s;
ampm.innerText=AMPM;
// 1000 mean 1 secand
setTimeout(() => {updateClock()
    
}, 1000); 

// setTimeout(updateClock,1000);
}